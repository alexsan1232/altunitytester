# Examples

We used AltUnity Tester to test some sample games to help you understand better how to use it.
We plan to add more examples in the near future.

```eval_rst

.. note::

    Example test projects below can be run on any platform.

```

**1.** Example test projects created for different languages and platforms:

* C# tests [Standalone Build(NuGetPackage) | ](https://gitlab.com/altom/altunity/examples/alttrashcat-tests-csharp)[Android Build | ](https://gitlab.com/altom/altunity/examples/android-build-with-csharp-tests)[iOS Build](https://gitlab.com/altom/altunity/examples/ios-build-with-csharp-tests)
* Python tests [Standalone Build | ](https://gitlab.com/altom/altunity/examples/standalone-build-with-python-tests)[Android Build | ](https://gitlab.com/altom/altunity/examples/alttrashcat-tests-python)[iOS Build ](https://gitlab.com/altom/altunity/examples/ios-build-with-python-tests)
* Java tests [Standalone Build | ](https://gitlab.com/altom/altunity/examples/standalone-and-android-build-with-java-tests)[Android Build | ](https://gitlab.com/altom/altunity/examples/standalone-and-android-build-with-java-tests)[iOS Build](https://gitlab.com/altom/altunity/examples/alttrashcat-tests---java)

    You can get the sample game from the [Unity Asset Store - link](https://assetstore.unity.com/packages/essentials/tutorial-projects/endless-runner-sample-game-87901).

**2.** Example test project for multiplayer features:

* [Multiplayer - iOS / Android | Python tests](https://gitlab.com/altom/altunity/examples/alttanksmultiplayer-test-python)

<!--
**3.** Example test project for AltUnity Pro Alpha:

* [WebGL - C# tests](https://gitlab.com/altom/altunity/examples/altunity-pro-alpha-example)

    You can get the sample game from the [Unity Asset Store - link](https://assetstore.unity.com/packages/essentials/tutorial-projects/tanks-tutorial-46209).
 -->

**3.** Example test project for AltUnity Tester:

* [SampleScenes](https://altom.gitlab.io/altunity/altunitytester/SampleScenes/SampleScenes.unitypackage)

    Import this package under your AltUnity Tester package in Unity, to have access to the sample scenes and C# tests.

**4.** Example test project for AltUnity Tester and Unity Test Framework:
* [Endless Runner - C# tests](https://gitlab.com/altom/altunity/examples/unity-test-runner)

    You can get the sample game from the [Unity Asset Store - link](https://assetstore.unity.com/packages/essentials/tutorial-projects/endless-runner-sample-game-87901).
